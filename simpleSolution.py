'''
Circle Problem Solution
Sonia Wong
Resources: https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
'''

from math import *

#Takes user input for points on the line
linePointA = input("Coordinate x,y: \n").split(',')
linePointA = [int(i) for i in linePointA]
x1 = linePointA[0]
y1 = linePointA[1]
linePointB = input("Coordinate x,y: \n").split(',')
linePointB = [int(i) for i in linePointB]
x2 = linePointB[0]
y2 = linePointB[1]

#Takes user input for Circle data
centerPoint = input("Circle Centerpoint Coordinate x,y: \n").split(',')
centerPoint = [int(i) for i in centerPoint]
centerX = centerPoint[0]
centerY = centerPoint[1]
radius = int(input("Radius: \n"))

distanceBetween = abs((y2 - y1) * centerX - (x2 - x1) * centerY + x2 * y1 - y2 * x1) / sqrt(((y2 - y1) ** 2) + ((x2 - x1) ** 2))

if (distanceBetween > radius):
    print("No Intersection")
elif (distanceBetween == radius):
    print("One Intersection")
else:
    print("Two Intersection")